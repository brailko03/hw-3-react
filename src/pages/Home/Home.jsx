import PropTypes from "prop-types";
import ProductList from "../../components/ProductList/ProductList";

export function Home({
  products,
  cartItems,
  favoriteItems,
  addToCart,
  addToFavorites,
  showCartModal,
  removeFromCart,
}) {
  return (
    <div>
      <ProductList
        products={products}
        cartItems={cartItems}
        favoriteItems={favoriteItems}
        showCartModal={showCartModal}
        addToCart={addToCart}
        addToFavorites={addToFavorites}
        removeFromCart={removeFromCart}
      />
    </div>
  );
}

Home.propTypes = {
  products: PropTypes.array,
  cartItems: PropTypes.array,
  favoriteItems: PropTypes.array,
  addToCart: PropTypes.func,
  addToFavorites: PropTypes.func,
  showCartModal: PropTypes.func,
  cartItemTotal: PropTypes.number,
  favoriteItemTotal: PropTypes.number,
  key: PropTypes.number,
  title: PropTypes.string,
  text: PropTypes.string,
  firstText: PropTypes.string,
  onClose: PropTypes.func,
};
