import "./Button.scss";

export default function Button({type, classNames, onClick, children}){

    return(
        <button className={classNames} type={type} onClick={onClick} >
            {children}
        </button>
    )
}
