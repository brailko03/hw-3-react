import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { NavLink } from "react-router-dom";
import "./Header.scss";

export default function Header({ cartItemTotal, favoriteItemTotal }) {
  return (
    <header className="header">
      <div className="container header-wrapper">
        <h1 className="title"> clothes for you</h1>
        <nav className="navbar">
          <ul>
            <li>
              <NavLink
                to="/"
                className={({ isActive }) => {
                  return isActive ? "active" : "";
                }}
              >
                Home
              </NavLink>
            </li>
            <li>
              <NavLink
                to="/cart"
                className={({ isActive }) => {
                  return isActive ? "active" : "";
                }}
              >
                Cart
              </NavLink>
              <div className="cart-shopping-wrapper">
            <FontAwesomeIcon icon={faCartShopping} size="lg" />{" "}
            <span>{cartItemTotal}</span>
          </div>
            </li>
            <li>
              {" "}
              <NavLink
                to="/favorites"
                className={({ isActive }) => {
                  return isActive ? "active" : "";
                }}
              >
                Favorites
              </NavLink>
              <div className="star-wrapper">
            <FontAwesomeIcon icon={faStar} size="lg" />{" "}
            <span>{favoriteItemTotal}</span>
          </div>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
}
