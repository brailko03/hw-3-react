import "./App.css";
import Header from "./components/Header/Header";
import { useState, useEffect } from "react";
import { Home, Cart, Favorites, NotFound } from "./pages";
import { Routes, Route } from "react-router-dom";
import ModalImage from "./components/Modal/ModalImage";
import ModalText from "./components/Modal/ModalText";

function App() {
  const [products, setProducts] = useState([]);

  const [cartItems, setCartItems] = useState(
    JSON.parse(localStorage.getItem("cartItems")) || []
  );
  const [favoriteItems, setFavoriteItems] = useState(
    JSON.parse(localStorage.getItem("favoriteItems")) || []
  );
  const [itemToRemove, setItemToRemove] = useState([]);

  const [showCartModal, setShowCartModal] = useState(false);

  const [showConfirmationModal, setShowConfirmationModal] = useState(false);

  const sendRequest = async (url) => {
    const response = await fetch(url);
    const products = await response.json();
    return products;
  };

  useEffect(() => {
    sendRequest("products.json").then((products) => {
      setProducts(products);
    });
  }, []);

  const handleAddToCart = (product) => {
    const indexItem = cartItems.findIndex((item) => item.id === product.id);

    if (indexItem === -1) {
      const newItem = {
        id: product.id,
        name: product.name,
        price: product.price,
        image: product.image,
        color: product.color,
        acticle: product.acticle,
        quantity: 1,
      };
      const mergedCartItems = [...cartItems, newItem];
      setCartItems(mergedCartItems);
      localStorage.setItem("cartItems", JSON.stringify(mergedCartItems));
    } else {
      const updatedItem = {
        ...cartItems[indexItem],
        quantity: cartItems[indexItem].quantity + 1,
      };
      const updatedCartItems = [...cartItems];
      updatedCartItems.splice(indexItem, 1, updatedItem);

      setCartItems(updatedCartItems);
      localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
    }
  };

  const handleAddToFavorites = (product) => {
    const indexItem = favoriteItems.findIndex((item) => item.id === product.id);

    if (indexItem === -1) {
      const newItem = {
        id: product.id,
        name: product.name,
        price: product.price,
        image: product.image,
      };
      const mergedFavoriteItems = [...favoriteItems, newItem];

      setFavoriteItems(mergedFavoriteItems);
      localStorage.setItem(
        "favoriteItems",
        JSON.stringify(mergedFavoriteItems)
      );
    } else {
      const updatedFavoriteItems = favoriteItems.filter(
        (item) => item.id !== product.id
      );
      setFavoriteItems(updatedFavoriteItems);
      localStorage.setItem(
        "favoriteItems",
        JSON.stringify(updatedFavoriteItems)
      );
      console.log(updatedFavoriteItems);
    }
  };

  const handleRemoveFromCart = (product) => {
    const updatedCartItems = [...cartItems];

    const indexItem = updatedCartItems.findIndex(
      (item) => item.id === product.id
    );

    if (indexItem !== -1) {
      updatedCartItems[indexItem].quantity -= 1;

      if (updatedCartItems[indexItem].quantity === 0) {
        updatedCartItems.splice(indexItem, 1);
      }

      setCartItems(updatedCartItems);
      localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
    }
  };

  const handleItemFavoriteClose = (product) => {
    const updatedFavoriteItems = favoriteItems.filter(
      (item) => item.id !== product.id
    );

    setFavoriteItems(updatedFavoriteItems);
    localStorage.setItem("favoriteItems", JSON.stringify(updatedFavoriteItems));
  };

  const handleToggleCartModal = () => {
    setShowCartModal(!showCartModal);
  };

  const handleToggleConfirmationModal = () => {
    setShowConfirmationModal(!showConfirmationModal);
  };

  const removeItemFromCart = (product) => {
    const updatedCartItems = cartItems.filter((item) => item.id !== product.id);

    setCartItems(updatedCartItems);
    localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
  };

  const handleRemoveClick = (item) => {
    setItemToRemove(item);
    handleToggleConfirmationModal();

  };

  const handlerConfirmRemove = () => {
    if (itemToRemove) {
      removeItemFromCart(itemToRemove);
    }
    handleToggleConfirmationModal();
  };

  const cartItemTotal = cartItems.length;

  const favoriteItemTotal = favoriteItems.length;

  return (
    <>
      <Header
        cartItemTotal={cartItemTotal}
        favoriteItemTotal={favoriteItemTotal}
      ></Header>

      <Routes>
        <Route
          path="/"
          element={
            <Home
              products={products}
              cartItems={cartItems}
              favoriteItems={favoriteItems}
              addToCart={handleAddToCart}
              addToFavorites={handleAddToFavorites}
              removeFromCart={handleRemoveClick}
              showCartModal={handleToggleCartModal}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <Cart 
            cartItems={cartItems} 
            handleRemoveClick={handleRemoveClick} />
          }
        />
        <Route
          path="/favorites"
          element={
            <Favorites
              favoriteItems={favoriteItems}
              handleItemFavoriteClose={handleItemFavoriteClose}
            />
          }
        />
        <Route path="*" element={<NotFound />} />
      </Routes>

      {showCartModal &&
        cartItems.map((product) => (
          <ModalText
            key={product.id}
            title={product.name}
            text={product.color}
            firstText={"Added to cart"}
            onClose={handleToggleCartModal}
          />
        ))}
      {showConfirmationModal &&
        cartItems.map((product) => (
          <ModalImage
            key={product.id}
            className={product.name}
            title={`${product.name} deleted`}
            text={`By clicking the “Yes, Delete” button, ${product.name} will be deleted.`}
            onConfirm={handlerConfirmRemove}
            onClose={handleToggleConfirmationModal}
          />
        ))}
    </>
  );
}

export default App;
